const burger = $('.burger-menu');

const hamburgerMotion = new TimelineMax({
    paused: true,
    progress: 0
})
.to('.burger-menu-line1', 0.3, {
    rotate:'45deg',
    top: '25px'
}, 0)
.to ('.burger-menu-line2', 0.3, {
    x:'+=100',
    opacity: 0
}, 0)
.to ('.burger-menu-line3', 0.3, {
    rotate: '-45deg',
    top: '25px'
}, 0)

.to(".nav-bg_rect", 0.3 ,{
    scale: 1,
    stagger: {
        amount: 0.15
    }
}, 0)
.to(".burger_text__close", 0.2, {
    translateY: "0%"
},0)
.to(".burger_text__open", 0.2, {
    translateY: "-100%"
}, 0)

.to('.navigation', 0.2 ,{
    translateX: 0,
}, 0)
.staggerTo('.nav__link', .2 , {
    translateY: 0,
    visibility: "visible"
},0)

burger.on("click", function() {
    if (hamburgerMotion.progress() === 0) {
        hamburgerMotion.play();
  
    } else {
        hamburgerMotion.reverse()
    }
});
        
// $('.navigation_links li').each(function (index, element) {
//     var tl = new TimelineLite({ paused: true });
//     tl.to(element, 0.2, {
//         scale: 1.5
//     }, 0)
//     // .to($(index).not(this), 0.2, {
//     //     color: "black"
//     // })
//     .to($('.navigation_links li').not(this), 0.2, {
//         scale: 0.9
//     }, 0)
//     element.animation = tl;
// })

// $(".nav__link").mouseenter(function () {
//     this.animation.play();
// })

// $(".nav__link").mouseleave(function () {
//     this.animation.reverse();
// })